#include<stdio.h>
#include <chrono>
#include <iostream>
#include <omp.h>


void single_thread()
{
	
	#define M 100

    double MA[M][M + 1], MAD;
	
    int i, j, v, k;



    /* ��������� ������ */
    for (i = 0; i < M; i++)
    {
        for (j = 0; j < M; j++)
        {
            if (i == j)
                MA[i][j] = 2.0;
            else
                MA[i][j] = 1.0;
        }
        MA[i][M] = 1.0 * (M)+1.0;
    }

    auto start = std::chrono::system_clock::now();

    /* ������ ��� */
    for (k = 0; k < M; k++)
    {
        MAD = 1.0 / MA[k][k];
        for (j = M; j >= k; j--)
            MA[k][j] *= MAD;
        for (i = k + 1; i < M; i++)
            for (j = M; j >= k; j--)
                MA[i][j] -= MA[i][k] * MA[k][j];

    }

    /* �������� ��� */
    for (k = M - 1; k >= 0; k--)
    {
        for (i = k - 1; i >= 0; i--)
            MA[i][M] -= MA[k][M] * MA[i][k];
    }

    /* ��������� ������� � ������ */
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    //printf("Time = %d\n", diff.count());
    std::cout << "Time= " << diff.count() << " s\n";

    /* ������ ������ ������ ������ */
    printf(" %f %f %f %f\n", MA[0][M], MA[1][M], MA[2][M], MA[3][M]);
    printf(" %f %f %f %f\n", MA[4][M], MA[5][M], MA[6][M], MA[7][M]);
}

void parallel_thread()
{
    omp_set_num_threads(2);
	
	#define M 200

    double MA[M][M + 1], MAD;

    int i, j, k;



    /* ��������� ������ */
    for (i = 0; i < M; i++)
    {
        for (j = 0; j < M; j++)
        {
            if (i == j)
                MA[i][j] = 2.0;
            else
                MA[i][j] = 1.0;
        }
        MA[i][M] = 1.0 * (M)+1.0;
    }

    auto start = std::chrono::system_clock::now();
	
	#pragma omp parallel
    {
        /* ������ ��� */
		#pragma omp for private (j,k, MAD)
        for (k = 0; k < M; k++)
        {
            MAD = 1.0 / MA[k][k];
            for (j = M; j >= k; j--)
                MA[k][j] *= MAD;
            for (i = k + 1; i < M; i++)
                for (j = M; j >= k; j--)
                    MA[i][j] -= MA[i][k] * MA[k][j];

        }
    }

	#pragma omp parallel
    {
    	
        /* �������� ��� */
		// #pragma omp for private (k)\
        for (k = M - 1; k >= 0; k--)
        {
            for (i = k - 1; i >= 0; i--)
                MA[i][M] -= MA[k][M] * MA[i][k];
        }
    }


    /* ��������� ������� � ������ */
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    //printf("Time = %d\n", diff.count());
    std::cout << "Time= " << diff.count() << " s\n";

    /* ������ ������ ������ ������ */
    printf(" %f %f %f %f\n", MA[0][M], MA[1][M], MA[2][M], MA[3][M]);
    printf(" %f %f %f %f\n", MA[4][M], MA[5][M], MA[6][M], MA[7][M]);
};

int main()
{
    // single_thread();
    parallel_thread();

    return 0;
}


