#include "quadrature.h"
#include "TimeCompute.h"

#include <iostream>
#include <cmath>

double f(double x)
{
	return 1 / (1 + x * x);
}

void quadrature(int n_)
{	
	double pi, sum = 0, term, h;
	int myrank, nprocs, n, i;
	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

	auto tc = TimeCompute();
	tc.start();

	if(myrank == 0)
	{
		//printf("Number of iterations=");
		//scanf_s("%d", &n);
		n = n_;
	}

	MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
	h = 1.0 / n;
	for (i = myrank + 1; i <= n; i += nprocs)
		sum += f(h * (i - 0.5));
	term = 4 * h * sum;
	MPI_Reduce(&term, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	// if (myrank == 0)
	// 	printf("Computed value of pi=%lg\n", pi);
	//
	tc.end();
	tc.print_diff();

	const long double PI = 3.141592653589793;
	std::cout.precision(17);
	std::cout << abs(PI - pi) << "\n";
	MPI_Finalize();
}

void trapez(int n_)
{
	double pi, sum = 0, term, h;
	int myrank, nprocs, n, i;
	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

	auto tc = TimeCompute();
	tc.start();

	if (myrank == 0)
	{
		//printf("Number of iterations=");
		//scanf_s("%d", &n);
		n = n_;
	}

	MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
	h = 1.0 / n;
	int a = 0;
	int b = 1;
	for (i = myrank + 1; i <= n - 1; i += nprocs)
		sum += f(a + i * h);
	term = 4 * h * ((((f(a) + f(b)) / 2) + sum));

	MPI_Reduce(&term, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	// if (myrank == 0)
	// 	printf("Computed value of pi=%lg\n", pi);
	//
	tc.end();
	tc.print_diff();

	const long double PI = 3.141592653589793;
	std::cout.precision(17);
	std::cout << abs(PI - pi) << "\n";
	MPI_Finalize();
}

void simpson(int n_)
{
	double pi, sum = 0, term, h;
	int myrank, nprocs, n, i;
	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

	auto tc = TimeCompute();
	tc.start();

	if (myrank == 0)
	{
		//printf("Number of iterations=");
		//scanf_s("%d", &n);
		n = n_;
	}

	MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
	h = 1.0 / n;
	int a = 0;
	int b = 1;
	
	for (i = myrank + 1; i <= n - 1; i += nprocs)
	{
		sum += f(a + i * h);
	}


	for (i = myrank + 1; i <= n; i += nprocs)
		sum += 2 * f(a + (i - 1 / 2) * h);

	term = 4 * (h / 3) * (sum);
	
	MPI_Reduce(&term, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	// if (myrank == 0)
	// 	printf("Computed value of pi=%lg\n", pi);
	//
	tc.end();
	tc.print_diff();

	const long double PI = 3.141592653589793;
	std::cout.precision(17);
	std::cout << abs(PI - pi) << "\n";
	MPI_Finalize();
}

void quadrature_run()
{
	//quadrature(10); // 1.36e-05 s  0.00083333141130559341
	//quadrature(50); // 1.52e-05 s 3.3333333209650817e-05
	//quadrature(100); // 1.64e-05 s 8.3333333322777037e-06
	//quadrature(500); // Time= 2.76e-05 s 3.3333333560037204e-07
	//quadrature(1000); // 4.12e-05 s 8.3333329570223214e-08
	quadrature(1000 * 1000 * 999); // 0.295979 s 6.2172489379008766e-14

	//trapez(10); // 1.24e-05 s 0.0016666646826344333
	 //trapez(50); // 1.09e-05 s 6.6666666539205721e-05
	 //trapez(100); // 1.6e-05 s 1.6666666664111318e-05
	 //trapez(500); // 2.65e-05 s 6.6666666276304909e-07
	 //trapez(1000); // 4.14e-05 s 1.6666666891040904e-07
	 //trapez(1000 * 1000 * 10); // 0.295907 s 1.936228954946273e-13

	//simpson(10); // 1.27e-05 s 0.1683333313493014
	 //simpson(50); // 1.7e-05 s 0.033399999999872865
	 //simpson(100); // 1.79e-05 s 0.016683333333330719
	//simpson(500); // 4.17e-05 s 0.0033340000000010583
	 //simpson(1000); // 6.84e-05 s 0.0016668333333336172
	//simpson(1000 * 1000 * 10); // 0.605042 s 1.666660853771873e-07
}