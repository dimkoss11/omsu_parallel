#include "TimeCompute.h"

#include <iostream>

TimeCompute::TimeCompute()
{
	diff_ = std::chrono::duration<double>();
}

std::chrono::system_clock::time_point TimeCompute::start()
{
	start_ = std::chrono::system_clock::now();
	return start_;
}

std::chrono::system_clock::time_point TimeCompute::end()
{
	end_ = std::chrono::system_clock::now();
	return end_;
}

std::chrono::duration<double> TimeCompute::diff()
{
	diff_ = end_ - start_;
	return diff_;
}

double TimeCompute::diff_time()
{
	diff_ = end_ - start_;
	return diff_.count();
}

void TimeCompute::print_diff()
{
	diff_ = end_ - start_;
	std::cout << "Time= " << diff_.count() << " s\n";
}
