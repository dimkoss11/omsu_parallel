//#include "packages/MSMPISDK.10.1.12498.18/Include/mpi.h"
#include <cstring>
#include <mpi.h>
#include <stdio.h>
#include "quadrature.h"



//todo https://www.opennet.ru/docs/RUS/linux_parallel/node138.html

void zad2(int argc, char* argv[])
{
	int myid, numprocs;
	myid = 10;
	numprocs = 10;
	fprintf(stdout, "Process %d of %d/n", myid, numprocs);
	MPI_Init(&argc, &argv);
	
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);

	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	
	MPI_Finalize();
}

void zad3(int argc, char* argv[])
{
	int myid, numorocs;
	char message[20];
	int myrank;
	MPI_Status status;
	int TAG = 0;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	if (myrank == 0)
	{
		strcpy_s(message, "Hi, Second Processor!");
		//MPI_Send(message, strlen(message) + 1, MPI_CHAR, myrank, TAG, MPI_COMM_WORLD);
		MPI_Send(message, strlen(message) + 1, MPI_CHAR, 1, TAG, MPI_COMM_WORLD);
	}
	else
	{
		//MPI_Recv(message, 100, MPI_CHAR, myrank, TAG, MPI_COMM_WORLD, &status);
		MPI_Recv(message, 100, MPI_CHAR, 0, TAG, MPI_COMM_WORLD, &status);
		printf("received: %s\n", message);
	}
	MPI_Finalize();	
}

void zadanie4(int argc, char* argv[])
{
	int myrank, size, message;
	int TAG = 0;
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	message = myrank;
	if ((myrank % 2) == 0)
	{
		if ((myrank + 1) != size)
		{
			MPI_Send(&message, 4, MPI_INT, myrank + 1, TAG, MPI_COMM_WORLD);
		}
	}
	else
	{
		if (myrank != 0)
		{
			MPI_Recv(&message, 100, MPI_INT, myrank, TAG, MPI_COMM_WORLD, &status);
		}
		printf("received : %i\n", message);
	}
	MPI_Finalize();
}

void example(int argc, char* argv[])
{

	// Initialize the MPI environment
	MPI_Init(NULL, NULL);
	// Find out rank, size
	int world_rank = 3;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	int world_size = 10;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// We are assuming at least 2 processes for this task
	if (world_size < 2) {
		fprintf(stderr, "World size must be greater than 1 for %s\n", argv[0]);
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	int number;
	if (world_rank == 0) {
		// If we are rank 0, set the number to -1 and send it to process 1
		number = -1;
		MPI_Send(
			/* data         = */ &number,
			/* count        = */ 1,
			/* datatype     = */ MPI_INT,
			/* destination  = */ 1,
			/* tag          = */ 0,
			/* communicator = */ MPI_COMM_WORLD);
	}
	else if (world_rank == 1) {
		MPI_Recv(
			/* data         = */ &number,
			/* count        = */ 1,
			/* datatype     = */ MPI_INT,
			/* source       = */ 0,
			/* tag          = */ 0,
			/* communicator = */ MPI_COMM_WORLD,
			/* status       = */ MPI_STATUS_IGNORE);
		printf("Process 1 received number %d from process 0\n", number);
	}
	MPI_Finalize();
}

void ping_pong(int argc, char* argv[])
{
	const int PING_PONG_LIMIT = 10;

	// Initialize the MPI environment
	MPI_Init(NULL, NULL);
	// Find out rank, size
	int world_rank = 2;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	int world_size = 4;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// We are assuming at least 2 processes for this task
	if (world_size != 2) {
		fprintf(stderr, "World size must be two for %s\n", argv[0]);
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	int ping_pong_count = 0;
	int partner_rank = (world_rank + 1) % 2;
	while (ping_pong_count < PING_PONG_LIMIT) {
		if (world_rank == ping_pong_count % 2) {
			// Increment the ping pong count before you send it
			ping_pong_count++;
			MPI_Send(&ping_pong_count, 1, MPI_INT, partner_rank, 0, MPI_COMM_WORLD);
			printf("%d sent and incremented ping_pong_count %d to %d\n",
				world_rank, ping_pong_count, partner_rank);
		}
		else {
			MPI_Recv(&ping_pong_count, 1, MPI_INT, partner_rank, 0, MPI_COMM_WORLD,
				MPI_STATUS_IGNORE);
			printf("%d received ping_pong_count %d from %d\n",
				world_rank, ping_pong_count, partner_rank);
		}
	}
	MPI_Finalize();
}

int main(int argc, char* argv[])
{
	// zad2(argc, argv);
	//zad3(argc, argv);
	//zad4(argc, argv);
	//example(argc, argv);
	//ping_pong(argc, argv);

	quadrature_run();
	
}
