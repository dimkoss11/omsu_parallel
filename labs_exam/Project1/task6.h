#pragma once

// 6..�������� ���������, ������� ��������� ������������ �� -
// ������ ��� ������ ���������� ���������� ������������
// ���� ��������.

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <mpi.h>

int task6(int argc, char* argv[])
{
	int iRoot = 0, iMyRank, iSize = 0, i;
	double dSum = 0, dResult = 0, Vec_1[20], Vec_2[20];
	MPI_Comm _Comm = MPI_COMM_WORLD; /* � ������ ��� �������� */
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(_Comm, &iMyRank);
	MPI_Comm_size(_Comm, &iSize);
	if (iMyRank == iRoot)
	{
		srand((int)(MPI_Wtime() * 1e4)); /* ���������� �������� */
		printf("Vector1=\n");
		for (i = 0; i < 20; ++i)
		{
			Vec_1[i] = rand() % 11;
			printf("%3.0f ", Vec_1[i]);
		}
		printf("\nVector2=\n");
		for (i = 0; i < 20; ++i)
		{
			Vec_2[i] = rand() % 11;
			printf("%3.0f ", Vec_2[i]);
		}
		printf("\n");
	}
	MPI_Bcast(Vec_1, 20, MPI_DOUBLE, iRoot, _Comm);
	MPI_Bcast(Vec_2, 20, MPI_DOUBLE, iRoot, _Comm);
	/* ������� ��������� ����� */
	for (i = iMyRank, dSum = 0; i < 20; i += iSize)
	{
		dSum += Vec_1[i] * Vec_2[i];
	} 
	MPI_Reduce(&dSum, &dResult, 1, MPI_DOUBLE, MPI_SUM, iRoot, _Comm);
	if (iMyRank == iRoot) printf("Result is %f\n", dResult);
	MPI_Finalize();

	return 0;
}