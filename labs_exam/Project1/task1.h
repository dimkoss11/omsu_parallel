#pragma once
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

// 1..�������� ���������, � ������� ���������� ������ ���
// ��������.� ����� �� ��� �������������� ��������� ��� -
// ������ �����, ���������� ������� ������ ������������.
// ������ ������� �������� ��� �����, ���������� ��, ���� -
// ���� ��������� � ������ �������.��������� ��������� ��
// �����.

int task1(int argc, char* argv[])
{
	int rank, size;
	int sum = 0;
	int* value;
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	srand((int)(MPI_Wtime() * 1e4));

	if (rank == 0)
	{
		scanf("%d", &size);
		value = new int[size];
		for (int i = 0; i <= size - 1; i++)
		{
			value[i] = int(rand() % 11);
			printf("%d ", value[i]);
		}
		MPI_Send(value, size, MPI_INT, 1, 0, MPI_COMM_WORLD);
		
		MPI_Recv(&sum, 1, MPI_INT, 1, 0,
			MPI_COMM_WORLD, &status);
		
		printf("The result is %d\n", sum);
	}

	if (rank == 1)
	{
		int count;
		
		MPI_Probe(0, 0, MPI_COMM_WORLD, &status);
		MPI_Get_count(&status, MPI_INT, &count);
		value = new int[count];
		MPI_Recv(value, count, MPI_INT, 0, 0,
			MPI_COMM_WORLD, &status);
		
		for (int i = 0; i < count; i++)
		{
			sum = sum + value[i];
		}

		MPI_Send(&sum, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
	}
	MPI_Finalize();

	return 0;
}
