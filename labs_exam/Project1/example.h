#pragma once
#include <cstdlib>
#include <mpi.h>

void example(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);
	MPI_Comm comm;
	int gsize, sendarray[100];
	sendarray[0] = 10;
	int root;
	// int* rbuf;
	int rbuf[400];
	root = 0;
	int rank;
	comm = MPI_COMM_WORLD;
	MPI_Comm_size(comm, &gsize);
	MPI_Comm_rank(comm, &rank);
	// rbuf = (int*)malloc(gsize * 100 * sizeof(int));
	MPI_Gather(sendarray, 100, MPI_INT, rbuf + rank, 100, MPI_INT, root, comm);

	MPI_Finalize();
}
