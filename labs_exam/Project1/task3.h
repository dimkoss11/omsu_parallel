#pragma once
// 3. �������� ���������, ������� ���������, � ����� ����� -
// �� �������������� �������� ���������.��� ����� ��� -
// �� ���������, ��� ��� ��������, �� ����������� ��������
// 0, �������� 100 ��������� �������� 0. ����� ������� 0
// ������������� ��������� � ������� �� ������, ���������
// MPI_ANY_SOURCE � MPI_ANY_TAG � MPI_Recv.

#include "mpi.h"
#include <stdio.h>
int task3(int argc, char* argv[])
{
	int rank; // ����������,���������� ��� ������ ��������, ���������� ��������� � ����� ��� ���������
	int size;
	int buf; 
	MPI_Status status; // c����� ���������
	MPI_Init(&argc, &argv); // ������������� MPI
	MPI_Comm_rank(MPI_COMM_WORLD, &rank); // ����������� ������ ��������
	MPI_Comm_size(MPI_COMM_WORLD, &size); // ����������� ���������� ���������
	if (rank == 0) // ���� ������� ��������
	{
		for (int i = 0; i < 100 * (size - 1); i++) // ���� ��� 100 ��������� * (���������� ��������� - �������� �������)
		{
			MPI_Recv(&buf, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status); // �������� ���������
			printf("message from process %d with  message tag %d\n", status.MPI_SOURCE, status.MPI_TAG); // ��������� ����������� � ���
		}
	}
	else
	{
		for (int i = 0; i < 100; i++)
		{
			MPI_Send(&buf, 1, MPI_INT, 0, i, MPI_COMM_WORLD); // �������� ��������� ��������� ��������
		}
	}
	MPI_Finalize(); // ���������� MPI
	return 0;

}