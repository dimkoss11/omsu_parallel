#pragma once

// 14. �������� ��������� ���������� ������������� ����� -
// ��� � ��� ������ �� ������� �����, ���������� �������� -
// ������� �� ���������.



#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <mpi.h>
#define LEN 1000

int task14(int argc, char* argv[])
{
	int iRoot = 0, iMyRank, iSize = 0, i;
	double dVal[LEN];
	MPI_Comm _Comm = MPI_COMM_WORLD; /* � ������ ��� �������� */
	struct { double value; int index; } In, Out;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(_Comm, &iMyRank);
	MPI_Comm_size(_Comm, &iSize);
	srand((int)(MPI_Wtime() * 1e4));
	for (i = 0; i < LEN; ++i)
	{
		dVal[i] = rand() * (double)((double)rand() / ((unsigned)rand() + 3));
		printf("%4.2f ", dVal[i]);
	}
	/* ����� ���������� �������� */
	In.value = dVal[0]; In.index = 0;
	for (i = 1; i < LEN; ++i)
		if (In.value < dVal[i])
		{
			In.value = dVal[i]; In.index = i;
		}
	In.index += iMyRank * LEN;
	MPI_Reduce(&In, &Out, 1, MPI_DOUBLE_INT, MPI_MAXLOC, iRoot, _Comm);
	if (iMyRank == iRoot)
		printf("\n\nMaximum is %f, found in process %i with index %i\n", Out.value,
			Out.index / LEN, Out.index % LEN);
	MPI_Finalize();

	return 0;
}