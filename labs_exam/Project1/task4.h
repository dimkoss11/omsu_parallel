#pragma once

#include <mpi.h>
#include<stdio.h>
#include <stdlib.h>

#include <Windows.h>
#include "Debugapi.h"
#include <Synchapi.h>

// 4. �������� ��������� ��� ��������� ������� �������� �� -
// ���������� ������ ������� �������� �� ������ ��������
// �������.��� ���������� ��������� �������� ������� : �� -
// ������� ����������� ���������� �������� ��������� ���
// ��������� ������� � �������� ����� �������(������� -
// ��� ������� ������ 100000 / size �������� ��� ����� size),
// ��������� ������������ ��������� ���(��������, 10) �
// ��������� ����������.

#define NUMBERS 10000000

int task4(int argc, char* argv[])
{
	// while (!::IsDebuggerPresent())
	// 	::Sleep(100); // to avoid 100% CPU load
	
	double t1, t2, tmin;
	int rank, size;
	double* numbers = new double[NUMBERS];
	MPI_Status status;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	srand((int)(MPI_Wtime() * 1e4));

	if (rank == 0)
	{
		// generate number
		for (int i = 0; i < NUMBERS; i++)
		{
			numbers[i] = rand();
		}
		
		t1 = MPI_Wtime();
		
		for (int i = 0; i < NUMBERS; i++)
		{
			MPI_Send(&numbers[i], 1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD);
		}

		t2 = MPI_Wtime();

		// double time_elapsed = (t2 - t1) / NUMBERS;
		double time_elapsed = t2 - t1;

		printf("time has elapsed: %lf", time_elapsed);
	}


	if (rank == 1)
	{
		// receive number
		for (int i = 0; i < NUMBERS; i++)
		{
			MPI_Recv(&numbers[i], 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
		}
	}

	MPI_Finalize();
	
	return 0;
}