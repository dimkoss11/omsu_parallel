#pragma once

// 13. �������� ���������, � ������� ������ ������� ������ -
// ���� ����� ������� ��������������� ����� � �������� ���
// ����� � ������� �������� ��������.
// 
#include <stdio.h>
#include <malloc.h>
#include <STDLIB.H>
#include <mpi.h>


int task13(int argc, char* argv[])
{
	int iGatherSize = 0, iRoot = 0, iMyRank, i, j;
	int sendarray[100][150], * RBuffer = 0;
	int* displs = 0, * rcounts = 0, stride = 255/*>100*/;
	MPI_Comm _Comm = MPI_COMM_WORLD; /* � ������ ��� �������� */
	MPI_Datatype stype;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(_Comm, &iMyRank);
	/* �������� �������� ����� �������� ������ ��� ������ */
	if (iMyRank == iRoot)
	{
		MPI_Comm_size(_Comm, &iGatherSize);
		RBuffer = (int*)malloc(stride * iGatherSize * sizeof(int));
		displs = (int*)malloc(iGatherSize * sizeof(int));
		rcounts = (int*)malloc(iGatherSize * sizeof(int));
		for (i = 0; i < iGatherSize; ++i)
		{
			displs[i] = stride * i; rcounts[i] = 100;
		}
	}
	srand((int)(MPI_Wtime() * 1e4));
	for (j = 0; j < 100; ++j)
		for (i = 0; i < 150; ++i) sendarray[j][i] = rand() % 999;
	MPI_Type_vector(100, 1, 150, MPI_INT, &stype);
	MPI_Type_commit(&stype);
	/* ���� ������ */
	MPI_Gatherv(sendarray, 1, stype, RBuffer, rcounts, displs, MPI_INT, iRoot, _Comm);
	/* ������� ������ �� ����� (��������� ������ ������� ��������,
	� ��������� iGatherSize == 0) */
	for (i = 0; i < iGatherSize; ++i)
	{
		printf("Process %i retuned:\n", i);
		for (j = 0; j < 100; ++j) printf("%4i ", RBuffer[i * stride + j]);
		printf("\n");
	}
	if (iMyRank == iRoot)
	{
		free(RBuffer); free(displs); free(rcounts);
	}
	MPI_Finalize();

	return 0;
}