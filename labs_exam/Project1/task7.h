#pragma once


// 7. �������� ���������, ������� ��������� ������������ �� -
// ������ ��� ������������ ������� �� �������.����� ��� -
// ����, ��� ������� � ������ ������������ � ������� ��� -
// �����, ����� ����������� ���� ���������.������ ��� -
// ���� ������� n / size ��������� ��������������� �������,
// ��� n ����������� ����� �������, size ������ ���������
// ����������.

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <mpi.h>
#define n 10
#define m 5

int task7(int argc, char* argv[])
{
	int iRoot = 0, iMyRank, iSize = 0, i, j;
	double dSum[n], dResult[n], Vec_1[m], Matr[m][n];
	MPI_Comm _Comm = MPI_COMM_WORLD; /* � ������ ��� �������� */
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(_Comm, &iMyRank);
	MPI_Comm_size(_Comm, &iSize);
	if (iMyRank == iRoot)
	{
		srand((int)(MPI_Wtime() * 1e4)); /* ���������� �������� */
		printf("Vector1=\n");
		for (i = 0; i < m; ++i)
		{
			Vec_1[i] = rand() % 11;
			printf("%2.0f ", Vec_1[i]);
		}
		printf("\nMatrix=\n");
		for (i = 0; i < m; ++i)
		{
			for (j = 0; j < n; ++j)
			{
				Matr[i][j] = rand() % 11;
				printf("%2.0f ", Matr[i][j]);
			}
			printf("\n");
		}
		printf("\n");
	}
	MPI_Bcast(Vec_1, m, MPI_DOUBLE, iRoot, _Comm);
	for (i = 0; i < n; ++i)
		MPI_Bcast(Matr[i], m, MPI_DOUBLE, iRoot, _Comm);
	/* ������� ��������� ����� */
	for (j = 0; j < n; ++j)
	{
		dSum[j] = 0;
		for (i = iMyRank; i < m; i += iSize)
			dSum[j] += Vec_1[i] * Matr[i][j];
	}
	MPI_Reduce(&dSum, dResult, n, MPI_DOUBLE, MPI_SUM, iRoot, _Comm);
	if (iMyRank == iRoot)
	{
		printf("Result is:\n");
		for (i = 0; i < n; ++i) printf("%3.0f ", dResult[i]);
	}
	MPI_Finalize();

	return 0;
}