//#include "packages/MSMPISDK.10.1.12498.18/Include/mpi.h"
#include <cstring>
#include <iostream>
#include <mpi.h>
#include <stdio.h>


#include "Gauss.h"
#include "GaussParall.h"
#include "MatrixVecMul.h"
#include "TimeCompute.h"

int main(int argc, char* argv[])
{
	// auto tc = TimeCompute();
	// tc.start();
	
	// matrix_vec_mul_par(argc, argv);
	// matrix_vec_seq();
	
	std::cout << "\n";
	// tc.end();
	// tc.print_diff();


	// gauss();
	gauss_strip_parallel(argc, argv);
	
}
