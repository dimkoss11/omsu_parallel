#include "MatrixMul.h"

//���������������� �������� ��������� ���� ���������� ������
static const int size = 5;
static double MatrixA[size][size];
static double MatrixB[size][size];
static double MatrixC[size][size];
static int i, j, k;

void matrix_mul_seq()
{
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            MatrixC[i][j] = 0;
            for (k = 0; k < size; k++) {
                MatrixC[i][j] = MatrixC[i][j] + MatrixA[i][k] * MatrixB[k][j];
            }
        }
    }
}