#include "single_integral.h"


#include <ctime>
#include <iostream>
#include <ostream>

// x^2
double f(const double x)
{
	return x * x;
}

void single_int()
{
    // Declares local constants
    // Inits local variables
    int n = 0;
    double sum = 0, x = 0, y = 0;
    // Seeds random number generator
    srand(time(NULL));

    // Gets the value of N
    //cout << "What is N? ";
    //cin >> n;
    n = 1000000;
    // Loops the given number of times

#pragma omp parallel private(x) reduction(+:sum) 
    {
#pragma omp for
        for (int i = 0; i < n; i++)
        {
            // Loops until criteria met
            while (true)
            {
                // Generates random points between 0 and 1
                x = static_cast<double>(rand()) / RAND_MAX + 1;
                // Checks if the points are suitable
                if (x >= 1 && x <= 2)
                {
                    // If so, break out of the while loop
                    break;
                }
            }
            // Updates our sum with the given points
            sum += f(x);
        }
    }

    std::cout << "The integral is " << sum / n << std::endl;
}
