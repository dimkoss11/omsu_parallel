//gcc -O3 -Wall -pedantic -fopenmp main.c
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <omp.h>

#include "pi_mc_openmp.h"

#include "single_integral.h"


long double pi_example()
{
    int i, throws = 99999, circleDarts = 0;
    long double randX, randY, pi;

    srand(time(NULL));

    for (i = 0; i < throws; ++i) {
        randX = rand() / (double)RAND_MAX;
        randY = rand() / (double)RAND_MAX;
        if (1 > ((randX * randX) + (randY * randY))) ++circleDarts;
    }

    long double four = 4.0;
	
    pi = four * (static_cast<long double>(circleDarts) / static_cast<long double> (throws));

    return pi;
}

using namespace std;

// The function to integrate
double f1(const double& x, const double& y)
{
    double numer = exp(-2 * x - 3 * y);
    double denom = sqrt(x * x + y * y + 1);
    double retval = numer / denom;
    return retval;
}

void integrate(size_t N)
{
    // Declares local constants
    const double area = atan(1); // pi/4
    // Inits local variables
    int n = 0;
    double sum = 0, x = 0, y = 0;
    // Seeds random number generator
    srand(time(NULL));
	
    // Gets the value of N
    //cout << "What is N? ";
    //cin >> n;
    n = N;
    // Loops the given number of times

#pragma omp parallel private(x, y) reduction(+:sum) 
	{
#pragma omp for
        for (int i = 0; i < n; i++)
        {
            // Loops until criteria met
            while (true)
            {
                // Generates random points between 0 and 1
                x = static_cast<double>(rand()) / RAND_MAX;
                y = static_cast<double>(rand()) / RAND_MAX;
                // Checks if the points are suitable
                if ((x * x + y * y) <= 1)
                {
                    // If so, break out of the while loop
                    break;
                }
            }
            // Updates our sum with the given points
            sum += f1(x, y);
        }
	}

    
    // Integral = area times mean value < f > of f
    sum = area * sum / n;
    cout << "The integral is " << sum << endl;
}


int main(int argc, char* argv[])
{
    // integrate(1000 * 1000 * 10);
    single_int();
    //std::cout << pi_example();
    //main1();
    return 0;
}
