#include "TimeCompute.h"

#include <iostream>

TimeCompute::TimeCompute()
{
	diff_ = std::chrono::duration<double>();
}

std::chrono::system_clock::time_point TimeCompute::start()
{
	return std::chrono::system_clock::time_point();
}

std::chrono::system_clock::time_point TimeCompute::end()
{
	return std::chrono::system_clock::time_point();
}

std::chrono::duration<double> TimeCompute::diff()
{
	diff_ = end_ - start_;
	return diff_;
}

double TimeCompute::diff_time()
{
	return diff_.count();
}

void TimeCompute::print_diff()
{
	std::cout << "Time= " << diff_.count() << " s\n";
}
