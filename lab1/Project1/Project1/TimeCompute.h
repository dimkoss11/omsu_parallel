#pragma once
#include <chrono>
class TimeCompute
{
	std::chrono::system_clock::time_point start_;
	std::chrono::system_clock::time_point end_;
	std::chrono::duration<double> diff_;
public:
	TimeCompute();
	std::chrono::system_clock::time_point start();
	std::chrono::system_clock::time_point end();
	std::chrono::duration<double> diff();
	double diff_time();
	void print_diff();
};

