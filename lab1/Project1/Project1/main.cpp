#include <chrono>
#include <iostream>
#include<omp.h>
#include <vector>

#include "utilities.h"
#include "examples.h"



int matvec(float** matrix, float* vector, float* result, int size_i, int size_j) {

    int i;
    int j;
	#pragma omp parallel shared(matrix,result,vector) private(i,j)
    {
		#pragma omp for  schedule(static)
        for (i = 0; i < size_i; i++) {
            result[i] = 0;
            for (j = 0; j < size_j; j++) {
                result[i] += matrix[i][j] * vector[j];
            }
        }
    }

    return 0;
}

void zad2() // ������� �� ������
{
    const int size_i = 5;
    const int size_j = 5;

    float** matrix = new float* [size_i];
    for (int i = 0; i < size_i; ++i)
        matrix[i] = new float[size_j];
	
    float* vector = new float[size_i];
    float* result = new float[size_i];

    for (int i = 0; i < size_j; ++i)
    {   
        result[i] = 0;
        vector[i] = i + 0.01 * i + 0.02;
    }

    for (size_t i = 0; i < size_j; i++)
    {
        std::cout << vector[i] << " ";
    }

    int counter = 0;
    std::cout << "\n";

    for (int i = 0; i < size_i; ++i)
    {
        counter++;
        for (int j = 0; j < size_j; ++j)
        {
            matrix[i][j] = counter + 0.01 * counter / (i + 0.5);
        }
    }

    for (size_t i = 0; i < size_j; i++)
    {
    	for (size_t j = 0; j < size_j; j++)
    	{
            std::cout << matrix[i][j] << " ";
    	}
        std::cout << "\n";
    }

   
    matvec(matrix, vector, result, size_i, size_j);

    for (size_t i = 0; i < size_j; i++)
    {
        std::cout << result[i] << " ";
    }
}

int** createMatrix(int size)
{
    auto** matrix = new int* [size];
    for (int i = 0; i < size; ++i)
        matrix[i] = new int[size];

    int counter = 0;

    for (int i = 0; i < size; ++i)
    {
        counter++;
        for (int j = 0; j < size; ++j)
        {
            matrix[i][j] = random_int();
        }
    }
    return matrix;
}


int** matrixMul(int** A, int** B, int size) {
    int i, j, k;
    int** res = createMatrix(size);
    omp_set_num_threads(4);
    print_console(A, size);
    print_console(B, size);
    auto start = std::chrono::system_clock::now();
	#   pragma omp parallel for private(k, j)
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            for (k = 0; k < size; k++) {
                res[i][j] += A[i][k] * B[k][j];
            }
        }
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    std::cout << "Time= " << diff.count() << " s\n";

    return res;
}

void zad3() // ������� �� �������
{

    int size = 5;
    int** result = matrixMul(createMatrix(size), createMatrix(size), size);
	
  

    print_console(result, size);

}



int main(int argc, char* argv[])
{
	// pi_computation();
    // par_for();
    // a1_5();
    // zad2();
    zad3();
    return 0;
}
