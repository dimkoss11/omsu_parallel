#include <ctime>
#include <cstdlib> 
#include <random>

#include "utilities.h"

#include <iostream>

int random_int()
{
    // srand(unsigned(time(0))); //use this seed
    int min = 1;
    int max = 50;
    // return min + (rand() % static_cast<int>(max - min + 1));
    std::random_device rd;     // only used once to initialise (seed) engine
    std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
    std::uniform_int_distribution<int> uni(min, max); // guaranteed unbiased

    auto random_integer = uni(rng);

    return random_integer;
}

void print_console(int** m, int size)
{
    std::cout << "\n";
    for (size_t i = 0; i < size; i++)
    {
        for (size_t j = 0; j < size; j++)
        {
            std::cout << m[i][j] << " ";
        }
        std::cout << "\n";
    }
}