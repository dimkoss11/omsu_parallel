#include<omp.h>
#include "examples.h"
#include <cstdio>


double compute_pi(const double y)
{
    return(4.0 / (1.0 + y * y));
}

void pi_computation()
{
    double w, x, sum, pi;
    int i;
    int n = 1000;
    w = 1.0 / n;
    sum = 0.0;
#pragma omp parallel for schedule(static,n/2) private(i,x) \
	shared(w) reduction(+:sum)
    for (i = 0; i < n; i++)
    {
        x = w * (i - 0.5);
        sum = sum + compute_pi(x);
    }
    pi = w * sum;
    printf("pi = %f\n", pi);

}

void par_for()
{
    int i = 0, a = 0, b = 0, n = 0;
    int x[10];
    int y[10];
    n = 10;
#pragma omp parallel for private(i)
#pragma omp shared(x, y, n) reduction(+: a, b)
    for (i = 0; i < n; i++)
    {
        a = a + x[i];
        b = b + y[i];
    }
}


void a1_1()
{
    int size, rank;

    /* �������� ��������� ������������ ��������� � � ������ �� ��� ��������
     * ���� ��������� ���������� size � rank */
#pragma omp parallel private(size, rank)
    {

        /* ������ ������� ������� ���� ���������� ����� � ������� ��� �� ����� */
        rank = omp_get_thread_num();
        printf("Hello World from thread = %d\n", rank);

        /* ������� ������� - master ������� �� ����� ���������� ��������� */
        if (rank == 0)
        {
            size = omp_get_num_threads();
            printf("Number of threads = %d\n", size);
        }

    }  /* ���������� ������������ ����� */

}

void a1_2() // ������������ ������������ ��������� ���� ��������.
{
    int   i, n;
    float a[100], b[100], sum;

    /* ������������� ��������� �������� */
    n = 100;
    for (i = 0; i < n; i++)
        a[i] = b[i] = i * 1.0;
    sum = 0.0;

    /* �������� ��������� ������������ ��������� � �����������������
     * ����� �� ������. ��� ������ �� ����� ��� �������� ���������� sum
     * ����������� �� ���� ���������. */
#pragma omp parallel for reduction(+:sum)
    for (i = 0; i < n; i++)
        sum = sum + (a[i] * b[i]);

    /* ������� ������� ������� �� ����� �������� sum */
    printf("   Sum = %f\n", sum);

}

/* ������������, � ������� ����������� �������� �������� */
float dotprod()
{

#define VECLEN 100
    float a[VECLEN], b[VECLEN], sum;


    int i, rank;
    sum = 0;

    rank = omp_get_thread_num();
#pragma omp for reduction(+:sum)
    for (i = 0; i < VECLEN; i++)
    {
        sum = sum + (a[i] * b[i]);
        printf(" rank = %d i=%d\n", rank, i);
    }
    return(sum);
}


void a1_3() // ������������ ������������ ��������� ���� ��������. 
{

#define VECLEN 100
    float a[VECLEN], b[VECLEN], sum;

    int i;

    /* ������������� ��������� �������� */
    for (i = 0; i < VECLEN; i++)
        a[i] = b[i] = 1.0 * i;
    sum = 0.0;

    /* �������� ��������� ������������ ��������� */
#pragma omp parallel
    sum = dotprod();

    printf("Sum = %f\n", sum);
}

void a1_4() // ������������ ������������ ��������� ���� ��������. 
{
#define N     50


    int i, size, rank;
    float a[N], b[N], c[N];

    /* ������������� ��������� �������� */
    for (i = 0; i < N; i++)
        a[i] = b[i] = i * 1.0;

    /* �������� ��������� ������������ ��������� */
#pragma omp parallel shared(a,b,c) private(i,rank,size)
    {

        /* ������ ������� ������� ���� ���������� ����� � ������� ��� �� ����� */
        rank = omp_get_thread_num();
        printf("Thread %d starting...\n", rank);

        /* ��������� ������� ������ */
#pragma omp sections nowait
        {
            /* ������ 0*/
#pragma omp section
            for (i = 0; i < N / 2; i++)
            {
                c[i] = a[i] + b[i];
                printf("rank = %d i= %d c[i]= %f\n", rank, i, c[i]);
            }
            /* ������ 1*/
#pragma omp section
            for (i = N / 2; i < N; i++)
            {
                c[i] = a[i] + b[i];
                printf("rank = %d i= %d c[i]= %f\n", rank, i, c[i]);
            }

        }  /* ���������� ����� ������ */

        if (rank == 0)
        {
            size = omp_get_num_threads();
            printf("Number of threads = %d\n", size);
        }

    }  /* ���������� ������������ ����� */

}

void a1_5() // ������ ������������� ��������� ������� �� ������.
{
#define M 10

    float A[M][M], b[M], c[M];
    int i, j, rank;

    /* ������������� ������ */
    for (i = 0; i < M; i++)
    {
        for (j = 0; j < M; j++)
            A[i][j] = (j + 1) * 1.0;
        b[i] = 1.0 * (i + 1);
        c[i] = 0.0;
    }
    printf("����� �������� ������� A � ������� b �� �����:\n");
    for (i = 0; i < M; i++)
    {
        printf("  A[%d]= ", i);
        for (j = 0; j < M; j++)
            printf("%.1f ", A[i][j]);
        printf("  b[%d]= %.1f\n", i, b[i]);
    }

    /* �������� ��������� ������������ ��������� � � ������ �� ��� ��������
     * ���� ��������� ���������� rank � i*/
#pragma omp parallel shared(A,b,c,total) private(rank,i)
    {
        rank = omp_get_thread_num();

        /* ��������� ����������������� ����� �� ������ */
#pragma omp for private(j)
        for (i = 0; i < M; i++)
        {
            for (j = 0; j < M; j++)
                c[i] += (A[i][j] * b[j]);

            /* ������ ������� ������� ���� ���������� �����, �������� ����� ����� �
             * �������� ��������������� ������� �� ������ ����� ����� � ������
             * ����������� ������ */
#pragma omp critical
            {
                printf(" rank= %d i= %d c[%d]=%.2f\n", rank, i, c[i]);
            }
        }   /* ����� ������������� ����� */
    } /* ���������� ������������ ����������� */


}